## Publicaciones

- Gutiérrez Alfaro, J., Sanabria Rodríguez, A., & Munguía Molina, D. (2017). Report of an enriching educational experience: Computer science undergrads as workshop facilitators in Costa Rican rural areas. 2017 XLIII Latin American Computer Conference (CLEI), 1-9.

- Munguía Molina, D., Gutiérrez Alfaro, J., & Sanabria Rodrıguez, A. (2017). Rethinking Laboratorio Experimental Towards a Methodological Transition. CLEI ELECTRONIC JOURNAL, 20(3).

- Rojas-Castro, J. E., Hernández-Víquez, J. M., Yock, J. P., & Sanabria Rodríguez, A. (2017). Jabirú: Hacia una herramienta para corrección asistida de errores. Memorias de congresos TEC.

- Sanabria Rodríguez, A. (2016). Selección de parámetros en Máquinas de Soporte Vectorial para identificación de polaridad [PhD Thesis]. Universidad de Costa Rica, Escuela de ciencias de la computación e informatica.

- Sanabria Rodríguez, A., & Casasola Murillo, E. (2017). Automatic parameterization of Support Vector Machines for Short Texts Polarity Detection. CLEI Electronic Journal. http://dx.doi.org/10.19153/cleiej.20.1.6

- Sanabria Rodríguez, A., & Casasola Murillo, E. (2016). Selección de parámetros en máquinas de soporte vectorial para identificación de polaridad. I Coloquio Costarricense de Procesamiento del Lenguaje Natural.

- Sanabria Rodríguez, A., Munguía Molina, D., & Gutierrez Alfaro, J. (2016). Laboratorio experimental: Lessons learned after two years encouraging undergraduate research in computer engineering. 2016 XLII Latin American Computing Conference (CLEI), 1-7. https://doi.org/10.1109/CLEI.2016.7833387

- Martínez de la Riva, Santiago, Quesada, Carmen, Pando de la Hoz, Francisco, Ayala-Orozco, Bárbara, Suárez-Mayorga, Ángela M., Mora, María, Vélez, Danny, Mendoza Gaytan, Jose Trinidad, Lins da Silva, Daniel, Koleff, Patricia, Quintero Rivero, Maria Esther, & Sanabria Rodríguez, Aurelio. (2013). Plinian Core: Integrating information about species. TDWG 2013 ANNUAL CONFERENCE. https://mbgocs.mobot.org/index.php/tdwg/2013/paper/view/516/0
