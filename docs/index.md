# Ing. Aurelio Sanabria, Msc

Es activista, investigador y profesor de [Ingeniería en computación](https://www.tec.ac.cr/escuelas/unidad-ingenieria-computacion-centro-academico-alajuela){:target="_blank"} del [Instituto Tecnológico de Costa Rica](http://www.tec.ac.cr/){:target="_blank"}

Con [bachillerato en Ingeniería en computación](https://www.tec.ac.cr/programas-academicos/bachillerato-ingenieria-computacion){:target="_blank"}) y  [Maestría Académica en computación e informática](https://pci.ucr.ac.cr/oferta-academica/maestria-academica){:target="_blank"}), ha laborado como docente para la institución desde el 2014 impartiendo los cursos de Introducción a la Programación, Taller de Programación, Fundamentos de Organización de Computadoras, Estructuras de datos, Lenguajes de Programación, Bases de datos I, Compiladores e Intérpretes, Sistemas Operativos, Recuperación de la Información Textual y Proyecto de ingeniería de software.

Adicionalmente colabora con la [Comunidad de Formación y Desarrollo Libre](https://www.instagram.com/comunidad_fdl/){:target="_blank"}, coordina el [Colectivo de investigación y acción directa en tecnología y cultura](http://tecu.gitlab.io/){:target="_blank"} y participa del grupo para la promoción de la ciencia abierta del [CSUCA](https://csuca.org/es/){:target="_blank"}.

## Datos curiosos del profesor
 
 - Es el único profesor con un personaje en **Leage Of Legends (LOL)**, "Aurelion Sol, el forjador de estrellas"
 - Luego de llevar mis cursos, se tendrá una nueva visión sobre el humor, los elefantes y los dinosaurios.

