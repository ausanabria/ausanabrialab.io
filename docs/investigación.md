# Investigación y acción directa 

El [Colectivo de investigación y acción directa en tecnología y cultura TECU ](https://tecu.gitlab.io){:target="_blank"} busca sensibilizar al público en general, especialmente a personas involucradas en la creación y gestión de tecnología, sobre las problemáticas relacionadas a las herramientas basadas en inteligencia artificial con un enfoque de género.

Su objetivo es el de participar activamente en la construcción colectiva de una visión ética y responsable a través de la investigación y la acción directa para la creación y gestión de las inteligencias artificiales manteniendo un enfoque de género.

- Síguenos en [Instragram](https://instagram.com/tecu_tec){:target="_blank"}

# Del zacate al papel

[*Del zacate al papel: Hacia una visión crítica de la computación*](https://zacate.gitlab.io/){:target="_blank"} es una publicación periódica que iniciamos para visibilizar las voces estudiantiles y potenciar su pensamiento crítico al cuestionarse su realidad inmediata, su contexto cultural y su futura profesión. 

