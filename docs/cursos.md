# Cursos 

##  Introducción a la programación

Este curso comprende una introducción a los conceptos básicos para modelar y solucionar problemas de forma algorítmica, así como las estrategias fundamentales para la construcción de programas de computadoras


##  Taller de programación

Este curso introduce los conceptos y herramientas necesarias para el desarrollo de programas, asimismo, complementa y ejercita las destrezas y las habilidades adquiridas en el curso de "Introducción a la Programación" con el fin de desarrollar experiencia en el desarrollo de programas


##  Estructuras de datos

El curso de Estructuras de Datos pretende brindar al estudiante la capacidad de abstracción y la comprensión de estructuras de datos y su uso para solucionar problemas.

Le da visión y las herramientas fundamentales para poder ingresar a tecnologías y temas computacionales más avanzados donde estos conocimientos son pilares

- [Videos](https://www.youtube.com/playlist?list=PL6uOuAMz7By2zDcEOXKElGqKGumMORj52){:target="_blank"}
- [Folleto: Los apuntes del profe](https://gitlab.com/cursos-itcr/los-apuntes-del-profe/estructuras-de-datos){:target="_blank"}



## Compiladores e interpretes

Este curso estudia principios y técnicas necesarios para la construcción de procesadores de lenguajes de programación, con énfasis en compiladores e intérpretes.  El aprendizaje permite aplicar modelos abstractos (lenguajes formales y autómatas), enfrentar problemas de manipulación de información simbólica, realizar programación modular avanzada, analizar sistemas complejos de software y hacer modificaciones sistemáticas, y ampliar el repertorio de métodos para resolución de problemas informáticos

- [Videos](https://www.youtube.com/playlist?list=PL6uOuAMz7By2SezT2iz50uADUsXX5grV6){:target="_blank"}
- [Folleto: Los apuntes del profe](https://gitlab.com/cursos-itcr/los-apuntes-del-profe/compiladores-e-interpretes/){:target="_blank"}

- Proyectos finales:
    - [II-2022](https://www.youtube.com/playlist?list=PL6uOuAMz7By1YUUYPObr9ugySiGOwbRj1){:target="_blank"}
    - [I-2022](https://www.youtube.com/playlist?list=PL6uOuAMz7By3U3Ri1eUPbhAfWgZR5VI9b){:target="_blank"}
    - [II-2021](https://www.youtube.com/playlist?list=PL6uOuAMz7By2X56qAiTwrKjYAKvnqdYkp){:target="_blank"}
    - [I-2021](https://www.youtube.com/playlist?list=PL6uOuAMz7By3Z9hDj0u-7kMw_UdUj2KWS){:target="_blank"}


## Sistemas operativos

El Curso presenta una breve reseña de lo que son los sistemas operativos, sus características y las funciones que tienen. 

Proporcionar una discusión completa de los fundamentos del diseño de los sistemas operativos, haciendo mención a las tendencias actuales.Además, crea un criterio en el estudiante sobre las decisiones que acarrean el diseño de un sistema operativo y el contexto en que éste opera


## Recuperación de la información textual

En este curso se introducen al estudiante los diferentes aspectos relacionados con el manejo automatizado de texto. Incluye temas como la representación de la información textual, algoritmos para su manejo y búsqueda, métricas de evaluación y herramientas disponible

- [Videos](https://www.youtube.com/playlist?list=PL6uOuAMz7By1Ck3K6qFBPFrHEycjqZ53g){:target="_blank"}

## Proyecto de ingeniería de software

El curso introduce los conceptos y técnicas necesarios para la creación de productos con tecnologías de tendencia reciente, así como los impactos arquitectónicos que estos puedan tener. El curso presta atención al enfoque de diseño centrado en el usuario, atendiendo especialmente lo concerniente al diseño de la experiencia del usuario. Los estudiantes trabajan en equipos que siguen un enfoque ágil que integra procesos, herramientas y técnicas de análisis, diseño, construcción, verificación y validación de software aprendidos a lo largo de la carrera, para lograr un producto expresado en tecnologías modernas que componen una arquitectura adecuada.  Aplica de manera práctica un proceso de desarrollo de software completo, pasando por todas las etapas – desde el estudio del problema y las necesidades de un cliente real, pasando por el diseño de una solución informática, hasta construir, integrar y probar el producto. Se desarrolla un sistema planteado por los estudiantes del curso, en grupos de 2 o 3 estudiantes.


- Proyectos finales:
    - [Póster académicos II-2022](https://www.youtube.com/playlist?list=PL6uOuAMz7By0gGXR-vdNrOfIFzMHXzUJi)
