## Actividades de difusión

Abajo una lista de participaciones en talleres, paneles y charlas

- Coach,  *"Competencia interuniversitaria de innovación abierta: UInnova"*. Cartago (2023). Ver [https://www.uinnovaevento.com/]()

- Comité científico, *"IV Jornadas Costarricenses en Investigación en Computación e Informática (JoCICI)"*. Costa Rica (2023). Ver [https://jocici2023.citic.ucr.ac.cr/](), 

- Ciclo de talleres *"Detección de sesgos de género en Integencia artificial"*. En conjunto con Tania María Sánchez Irola,  Valeria Quesada Benavides, Laura Molina Cordero, Natalia Ramírez Arguedas, Luis Diego Mora Aguilar e Isaac Cordero Tapia.  Cartago (Costa Rica), 2023

- Panel *"¿Presencialidad vs virtualidad?"* [disponible aquí](https://www.youtube.com/watch?v=kM66Q3RT6ic&list=PL6uOuAMz7By3TLsOkqC3dOJJiMrebdAGy&index=3){:target="_blank"}. En conjunto con . Alajuela (Costa Rica), 2022

- Panel *"¿Se puede medir el conocimiento?"* [disponible aquí](https://www.youtube.com/watch?v=gN229PM6xaI&list=PL6uOuAMz7By3TLsOkqC3dOJJiMrebdAGy&index=4){:target="_blank"}. En conjunto con Steven Alvarado, Daniela Paniagua, Victor Barquero, Yirlany Fonseca, Diego Munguía y Mauricio Avilés . Alajuela (Costa Rica), 2022

- Panel *"¿Profes difíciles o cursos difíciles?"* [disponible aquí](https://www.youtube.com/watch?v=xLayCH7-gVo&list=PL6uOuAMz7By3TLsOkqC3dOJJiMrebdAGy&index=5){:target="_blank"}. En conjunto con Victor Barquero, Daniel Blanco y María Estrada . Alajuela (Costa Rica), 2022

- Participación en el panel: *“Investigación de género en la IA: Adquiriendo nuevos conocimientos”* [disponible Aquí](https://www.youtube.com/watch?v=N3pm738SMvg){:target="_blank"}. En conjunto con Tania María Sánchez Irola,  Valeria Quesada Benavides y Jonathan Quesada Salas. Cartago (Costa Rica), 2022

- Taller *“Introducción al procesamiento del lenguaje natural”*, Alajuela (Costa Rica), 2021

- Panel *"Los exámenes cómo herramientas para aprender"* [disponible aquí](https://www.youtube.com/watch?v=7I_8GuIlmFs&list=PL6uOuAMz7By3TLsOkqC3dOJJiMrebdAGy&index=1&t=1s){:target="_blank"}. En conjunto con Josué Castro Ramírez, Kenneth Vargas, Diego Munguía Molina, Walter Álvarez y Yanina Contreras. Alajuela (Costa Rica), 2021

- I primer ciclo de charlas relámpago *Introducción al Procesamiento de Lenguaje Natural*, Alajuela (Costa Rica), 2019 

- Ciclo de talleres: *"Cartografía libre*" en el marco del mes de la ciencia y la tecnología organizado por el MICITT, En conjunto a Jaime Gutiérrez Alfaro, Diego Munguía Molina y multitud de maravillosas personas estudiantes. Cañas, Suretka y Upala (Costa Rica), 2016

- Taller *"Scripting utilizando BASH (Terminal de GNU/Linux)"*, Chipré (Panamá), 2014

- Taller *"Weka aplicado a la detección de polaridad"*, San José (Costa Rica), 2014 

- Taller *“Training course on biodiversity data publishing and fitness-for-use in the GBIF Network”*. Buenos Aires (Argentina), Setiembre 2011.

- Taller *“Instalación, administración y uso de herramientas de software necesarias para digitalización y publicación de datos de biodiversidad”*.  Timpú (Bután), Enero 2010.

- Taller *“Sharing the Experiences of Developing and Using Biodiversity Information Management System in the Developing World: the INBio Experience”*.  San José (Costa Rica), Julio 2009.

- Taller *“Instalación y administración de las herramientas de software desarrolladas por la REyE”*.  San José (Costa Rica), Agosto 2008.
