# Acerca de Aurelio

Si desea contactar al profesor puede enviarle un correo a la dirección [ausanabria@itcr.ac.cr](mailto:ausanabria@itcr.ac.cr) o vía [Mensaje directo en Telegram](https://t.me/sufrostico).

## Perfiles académicos

- *Orcid*: <div itemscope itemtype="https://schema.org/Person"><a itemprop="sameAs" content="https://orcid.org/0000-0002-2998-3587" href="https://orcid.org/0000-0002-2998-3587" target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon">https://orcid.org/0000-0002-2998-3587</a></div>

- *Google Scholar*: <div itemscope itemtype="https://schema.org/Person"><a itemprop="sameAs" content="https://scholar.google.com/citations?user=SbnLsBkAAAAJ&hl=en" href="https://scholar.google.com/citations?user=SbnLsBkAAAAJ&hl=en" target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;"><img src="https://scholar.google.com/favicon.ico"
 style="width:1em;margin-right:.5em;" alt="Google Scholar icon"> https://scholar.google.com/citations?user=SbnLsBkAAAAJ&hl=en </a></div>


- *Zotero*: <div itemscope itemtype="https://schema.org/Person"><a itemprop="sameAs" content="https://www.zotero.org/ausanabria/" href="https://www.zotero.org/ausanabria/ " target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;"><img src="https://www.zotero.org/favicon.ico"
 style="width:1em;margin-right:.5em;" alt="Zotero icon"> https://www.zotero.org/ausanabria/</a></div>


## Datos curiosos del profesor
 
 - Es el único profesor con un personaje en **Leage Of Legends (LOL)**, "Aurelion Sol, el forjador de estrellas"

 - Luego de uno de mis cursos, se tendrá una nueva visión sobre el humor, los elefantes y los dinosaurios.

