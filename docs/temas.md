# Otros temas de interés

## Ciencia abierta

> 8. El  término  “ciencia  abierta”  se  refiere  a  un  concepto  general  que  combina  diversos  movimientos  y  prácticas  con  el  fin  de  que  los  conocimientos,  los  métodos,  los  datos  y  las  pruebas de carácter científico estén disponibles libremente y sean accesibles para todos, se incrementen las colaboraciones científicas y el intercambio de información en beneficio de la ciencia y la sociedad, y se abra el proceso de creación y difusión de conocimientos científicos a los agentes sociales que no pertenecen a la comunidad científica institucionalizada
>
> [...]
>
> 15. Los valores fundamentales de la ciencia abierta se derivan de las implicaciones éticas, epistemológicas  y  sociotecnológicas  de  la  apertura  de  la  ciencia  a  la  sociedad  y  de  la  ampliación de estos principios de apertura a todo el ciclo de la investigación científica. Estos valores son los siguientes:
> 
> i) el beneficio colectivo: como bien público mundial, la ciencia abierta pertenece a la humanidad en común y beneficia a la humanidad en su conjunto; 
> 
> ii) la  equidad  y  la  justicia:  la  ciencia  abierta  debería  contribuir  en  gran  medida  a  garantizar  la  equidad  entre  los  investigadores  de  los  países  desarrollados  y  los  países en desarrollo, permitir el intercambio justo y recíproco de las aportaciones y  los  productos  científicos,  y  asegurar  la  igualdad  de  acceso  al  conocimiento  científico   tanto   a   los   productores   como   a   los   usuarios   de   conocimientos,   independientemente   de   su   ubicación   geográfica,   género,   origen   étnico   o   circunstancias socioeconómicas; 
> 
> iii) la calidad y la integridad: la ciencia abierta debería favorecer la investigación de alta  calidad  mediante  la  utilización  de  múltiples  fuentes  de  conocimiento  y  una  amplia  disponibilidad  de  los  datos  y  métodos  de  investigación  con  miras  a  la  realización de controles y exámenes rigurosos; 
> 
> iv)  la  diversidad:  la  ciencia  abierta  debería  abarcar  una  diversidad  de  prácticas,  flujos de trabajo, lenguas, resultados y temas de investigación que se ajusten a las  necesidades  y  el  pluralismo  epistémico  de  las  diversas  comunidades  de investigación,  académicos,  poseedores  de  conocimientos  y  agentes  sociales  de  diferentes países y regiones;
> 
> v) la inclusión: en la búsqueda común de nuevos conocimientos, la ciencia abierta debería  hacer  participar  de  manera  significativa  al  conjunto  de  la  comunidad  científica,  así  como  el  público  en  general  y  los  depositarios  de  conocimientos  externos  a  la  comunidad  científica  oficial,  en  particular  los  pueblos  indígenas  y  otras comunidades tradicionales.



- Tomado de [Anteproyecto de Recomendación de la UNESCO sobre la Ciencia Abierta](https://unesdoc.unesco.org/ark:/48223/pf0000374837_spa){:target="_blank"}

## Software libre

> El software libre es un tipo de programas de ordenador que respeta nuestra libertad. Utilizar software libre es una decisión política y ética que nos permite ejercer nuestro derecho a aprender y a compartir lo que aprendemos con otras personas.
>
> Es habitual que el software que compramos nos niegue dichos derechos. Eso es porque realmente no estamos adquiriendo la propiedad del software que compramos, sino una licencia sobre el uso del software. Y dichas licencias nos atan mediante numerosas y sutiles reglas acerca de lo que podemos y no podemos hacer con el programa.
>
> Si hacemos una copia del programa y se la damos a un amigo, si tratamos de averiguar cómo funciona el programa, si tenemos más de una copia del programa en nuestro computador en casa... y nos sorprenden haciéndolo, podemos ser multados o incluso procesados. Eso es lo que pone en la letra pequeña de las licencias.
>
> [...]
>
> ¿Cómo funciona? ¡Copyleft!
>
> En vista a que las leyes de copyright sobre el software suelen utilizarse para privarnos de nuestra libertad, Stallman y la FSF desarrollaron un documento legal llamado la GNU General Public License (GPL) para proteger dicha libertad. En lugar de restringir qué puede hacerse con el software, la GPL nos alienta a estudiar y compartir. Por eso es lo que denominamos una licencia "copyleft". Miles de personas y negocios, desde amateurs a grandes compañías como IBM y Novell, desarrollan y distribuyen software bajo los términos de la GPL.
>
> Porque utilizar el software es una elección del usuario, no de la gente que lo programa y lo vende. Es fácil perder nuestra libertad al pulsar el botón Aceptar después de pasar rápidamente treinta páginas de restricciones. O podemos pulsar Cancelar, y buscar algún programa libre que haga lo que necesitamos.

- Tomado de [¿Qué es el software libre? - www.fsf.org](https://www.fsf.org/es/recursos/que-es-el-software-libre){:target="_blank"}


## Pedagogías libertarias

Un libro maravilloso para introducirte al tema 

- Filosofía de la educación, Judith de Suissa

## Paulo Freire y su pedagogía

Paulo Freire fue un pedagogo latinoamericano que desarrollo la pedagogía de la liberación, si quieres saber más puedes empezar por el video  [La letra con sangre entra - Paulo Freire: Educación liberadora](https://www.youtube.com/watch?v=VEQPBTM1OWE){:target="_blank"}

## Decolonialidad

El pensamiento decolonial es necesario para el desarrollo de nuestra región y para iniciar con el tema puedes escuchar a Enrique Dussel presentando el tema de [La decolonización cultural](https://www.youtube.com/watch?v=Q86_LPat-IQ){:target="_blank"}
